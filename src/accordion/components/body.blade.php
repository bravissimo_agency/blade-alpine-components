<div 
    {{ $attributes }}
    x-show="isOpen"
    :id="id"
    :aria-labelledby="headerId"
    role="region"
    x-cloak
    x-collapse
>
    {!! $slot !!}
</div>

