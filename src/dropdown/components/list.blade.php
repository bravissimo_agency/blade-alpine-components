<ul 
    x-show="isOpen"
    {{ $attributes->merge(['class' => 'dropdown__list']) }}
    x-transition.duration.250ms
    @click.outside="close"
    x-cloak    
>
    {!! $slot !!}
</ul>