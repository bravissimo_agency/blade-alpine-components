import accordion from './accordion';

export default (Alpine) => {
    Alpine.data('accordion', accordion);
}