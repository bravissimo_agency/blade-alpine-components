<button
    type="button"
    :class="{ 'isOpen': isOpen }"
    @click="toggle"
    {{ $attributes->merge(['class' => 'shareButton']) }}
>
    {!! $slot !!}
</button>