@props([
    'label'
])

<div
    role="checkbox"
    tabindex="0"
    :aria-checked="isActive ? 'true' : 'false'"
    :class="{ 'isActive': isActive }"
    x-modelable="isActive"
    @click="toggle"
    @keydown.prevent.enter="toggle"
    @keydown.prevent.space="toggle"
    x-data="checkbox"
    {{ $attributes->merge(['class' => 'checkbox flex items-center']) }}
>
    <div class="checkbox__icon relative flex-shrink-0">
        <span class="checkbox__dot"></span>
    </div>
    
    <div class="checkbox__label">
        {!! $label ?? $slot !!}
    </div>
</div>
