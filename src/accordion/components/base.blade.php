@props([
    'id' => substr(md5(mt_rand()), 0, 7),
    'defaultOpen' => null
])

<div 
    x-data="accordion({
        id: '{{ $id }}',
        defaultOpen: @json($defaultOpen ?? false)
    })"
    :class="{ 'isOpen': isOpen }"
    {{ $attributes }}
>
    {!! $slot !!}
</div>