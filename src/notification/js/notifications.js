export default () => ({
    notifications: [],

    showCookieNotification: window.localStorage.getItem('cookieInformationAccepted') === null,

    init () {
        Alpine.effect(() => {
            this.notifications = this.$store.notifications.items;
        });
    },

    closeCookieNotification () {
        this.showCookieNotification = false;
        window.localStorage.setItem('cookieInformationAccepted', true);
    },

    close (notification) {
        this.$store.notifications.remove(notification);
    }
});