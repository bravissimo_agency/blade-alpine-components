export default ({ defaultOpen, id }) => ({
    id: id,

    defaultOpen: defaultOpen || false,

    isOpen: defaultOpen || false,

    headerId: `header__${id}`,

    toggle() {
        this.isOpen = !this.isOpen;
    },

    open () {
        this.isOpen = true;
    },

    close () {
        this.isOpen = false;
    }
});