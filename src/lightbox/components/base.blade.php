@props([
    'images',
    'settings' => null
])

<div
    x-data="lightbox({
        images: {{ json_encode($images ?? null) }},
        settings: {{ json_encode($settings ?? null) }}
    })"
    {{ $attributes }}
>
    {!! $slot !!}
</div>

@once
    @push('bottom')
        <x-lightbox::pswp />
    @endpush
@endonce