import carousel from './carousel';

export default (Alpine) => {
    Alpine.data('carousel', carousel);
}