@props([
    'name',
])

<input
    id="{{ $name }}"
    name="{{ $name }}"
    {{ $attributes->merge(['class' => 'input', 'type' => 'text']) }}
>
