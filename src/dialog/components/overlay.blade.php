<div 
    @click="close"
    {{ $attributes }}
>
    {!! $slot ?? null !!}
</div>