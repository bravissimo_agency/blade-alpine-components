@props([
    'coordinates',
    'zoom' => 10,
    'markerWidth' => 38,
    'markerHeight' => 61,
    'markerIcon' => asset('images/marker.svg'),
    'mapStyle' => [],
])

<div 
    x-data="googleMaps({
        coordinates: {{ json_encode($coordinates) }},
        mapStyle: {{ json_encode($mapStyle) }},
        markerWidth: {{ json_encode($markerWidth) }},
        markerHeight: {{ json_encode($markerHeight) }},
        zoom: {{ json_encode($zoom) }},
        markerIcon: '{{ $markerIcon }}',
        apiKey: '{{ config('services.google_maps_api_key') }}'
    })"
    class="googleMap ab100"
></div>