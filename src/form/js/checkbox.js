export default () => ({
    isActive: false,

    toggle () {
        this.isActive = !this.isActive;

        this.$dispatch('changed', this.isActive);
    }
})