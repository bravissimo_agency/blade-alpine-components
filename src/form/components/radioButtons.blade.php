@props([
    'items',
    'optionLabel' => 'label',
    'optionValue' => 'value'
])

<div 
    x-data="radioButtons"
    x-modelable="selected"
    role="radiogroup"
    {{ $attributes->merge(['class' => 'radioButtons']) }}
>
    @foreach ($items as $item)
        <div
            role="radio"
            :class="{ 'isSelected': selected === '{{ $item->{$optionValue} }}' }"
            :aria-checked="selected === '{{ $item->{$optionValue} }}'"
            class="radioButton"
            tabindex="0"
            @keydown.prevent.enter="select('{{ $item->{$optionValue} }}')"
            @keydown.prevent.space="select('{{ $item->{$optionValue} }}')"
            @click="select('{{ $item->{$optionValue} }}')"
        >
            <span class="radioButton__icon"></span>

            <span class="radioButton__label">
                {{ $item->{$optionLabel} }}
            </span>
        </div>
    @endforeach
</div>