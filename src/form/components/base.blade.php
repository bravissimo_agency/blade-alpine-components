@props([
    'endpoint',
    'mustAcceptTerms' => false,
    'initialData' => null,
    'gtmData' => null,
    'useJson' => false
])

<form
    x-data="form({
        endpoint: '{{ $endpoint }}',
        errorMessage: '{{ config('form.error_message') }}',
        termsMessage: '{{ config('form.terms_message') }}',
        mustAcceptTerms: @json($mustAcceptTerms),
        recapatcha: '{{ config('services.recaptchav3.site_key') }}',
        initialData: {{ json_encode($initialData) }},
        gtmData: {{ json_encode($gtmData) }},
        useJson: @json($useJson),
    })"
    @submit.prevent="submitHandler"
    {{ $attributes->merge(['class' => 'form']) }}
>
    {!! $slot !!}
</form>
