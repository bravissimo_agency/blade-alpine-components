<div 
    x-data="shareButton"
    @click.outside="close"
    {{ $attributes->merge(['class' => 'shareButtonHolder relative']) }}
>
    {!! $slot !!}
</div>