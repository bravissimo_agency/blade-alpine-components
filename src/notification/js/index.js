import notifications from './notifications';

export default (Alpine) => {
    Alpine.data('notifications', notifications);

    Alpine.store('notifications', {
        items: [],

        add ({message, status}) {
            const id = Date.now();

            this.items.push({
                id,
                message,
                status,
                visible: false
            });

            this.startTimer(id);
        },

        remove (notification) {
            notification.visible = false;
            
            setTimeout(() => {
                const index = this.items.indexOf(notification);

                if (index === -1) {
                    return;
                }

                this.items.splice(index, 1);
            }, 350);
        },

        startTimer(id) {
            const notification = this.items.find(notification => notification.id === id);

            requestAnimationFrame(() => notification.visible = true);

            setTimeout(() => {
                if (notification) {
                    this.remove(notification);
                }
            }, 6000);
        }
    })
}