import shareButton from './shareButton';

export default (Alpine) => {
    Alpine.data('shareButton', shareButton);
}