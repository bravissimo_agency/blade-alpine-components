@props([
    'cookieMessage'
])

<div
    x-data="notifications"
    class="notificationContainer"
    role="dialog"
    x-cloak
>
    <template 
        x-for="notification in notifications"
        :key="notification.id"
    >
        <x-notification::notification />
    </template>

    @if (!empty($cookieMessage))
        <x-notification::cookie :message="$cookieMessage" />
    @endif
</div>
