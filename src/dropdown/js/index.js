import dropdown from './dropdown';

export default (Alpine) => {
    Alpine.data('dropdown', dropdown);
}