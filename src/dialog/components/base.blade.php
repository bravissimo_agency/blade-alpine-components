@props([
    'id',
])

<div
    x-data="dialog('{{ $id }}')"
    x-show="isOpen"
    x-cloak
    {{ $attributes }}
>
    {!! $slot !!}
</div>
