<span {{ $attributes->merge(['class' => 'shareButtonIcon']) }}>
    {!! $slot !!}
</span>