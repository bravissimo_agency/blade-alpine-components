<button 
    {{ $attributes }}
    type="button"
    :id="headerId"
    :aria-expanded="isOpen.toString()"
    :aria-controls="id"
    @click="toggle"
>
    {!! $slot !!}
</button>