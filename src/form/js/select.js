export default ({ items, optionLabel, optionValue, multiple }) => ({
    items: items,
    optionLabel: optionLabel,
    optionValue: optionValue,
    multiple: multiple,

    isOpen: false,
    selected: multiple ? [] : null,
    selectedLabel: null,
    selectedValue: null,

    init () {
        this.setInitialItem();

        document.addEventListener('keydown', (event) => {
            if (!this.isOpen) {
                return;
            }

            if (event.key === 'Escape') {
                this.close();
            }
        });
    },

    clear () {
        this.selected = null;
        this.selectedLabel = null;
        this.selectedValue = null;
    },

    setInitialItem () {
        setTimeout(() => {
            if (this.selectedValue) {
                const item = this.items.find(item => item[this.optionValue] == this.selectedValue);

                if (item) {
                    this.selectItem(item);
                }
            }
        }, 100);
    },

    selectItem (item) {
        if (!this.multiple) {
            this.selected = item;

            this.selectedLabel = item[this.optionLabel];
            this.selectedValue = item[this.optionValue];

            this.close();

            return;
        }

        const selectedItem = this.selected.find(i => i[this.optionValue] === item[this.optionValue]);

        if (selectedItem) {
            const index = this.selected.indexOf(selectedItem);
            this.selected.splice(index, 1)
        } else {
            this.selected.push(item);
        }

        this.selectedLabel = this.selected.map(i => i[this.optionLabel]).join(', ');
        this.selectedValue = this.selected.map(i => i[this.optionValue]).join(', ');
    },

    isActive (item) {
        if (!this.multiple) {
            return item[this.optionValue] === this.selectedValue;
        }

        return this.selected.find(i => i[this.optionValue] === item[this.optionValue]);
    },

    toggle() {
        this.isOpen = !this.isOpen;
    },

    open () {
        this.isOpen = true;
    },

    close () {
        this.isOpen = false;
    }
});