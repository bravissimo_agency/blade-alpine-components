<x-form::checkbox 
    {{ $attributes }}
    @changed="(value) => termsAccepted = value.detail"
>
    {!! $slot !!}
</x-form::checkbox>