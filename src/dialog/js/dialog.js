export default (id) => ({
    isOpen: false,

    init () {
        Alpine.effect(() => {
            if (this.$store.dialog.id === id) {
                this.isOpen = true;
                document.body.style.overflow = 'hidden';
            } else {
                this.isOpen = false;
                document.body.style.overflow = '';
            }
        });

        document.addEventListener('keydown', (event) => {
            if (!this.isOpen) {
                return;
            }

            if (event.key === 'Escape') {
                this.close();
            }
        });
    },

    close () {
        this.$store.dialog.close();
    },

    open (id) {
        this.$store.dialog.open(id);
    }
});