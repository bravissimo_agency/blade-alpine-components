import lightbox from './lightbox';

export default (Alpine) => {
    Alpine.data('lightbox', lightbox);
}