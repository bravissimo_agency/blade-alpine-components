@props([
    'name',
])

<textarea 
    id="{{ $name }}"
    name="{{ $name }}"
    {{ $attributes->merge(['class' => 'textarea input']) }}
></textarea>
