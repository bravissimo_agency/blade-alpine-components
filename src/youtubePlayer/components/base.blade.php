@props([
    'id',
    'url'
])

<x-dialog::trigger 
    :id="$id"
    {{ $attributes }}
>
    {!! $slot !!}
</x-dialog::trigger>

@push('bottom')
    <x-dialog::base 
        :id="$id"
        class="youtubePlayerDialog"
    >
        <x-dialog::overlay class="youtubePlayerDialog__overlay ab100" />

        <x-dialog::close class="youtubePlayerDialog__closeButton">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="youtubePlayerDialog__closeButtonIcon"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
        </x-dialog::close>

        <x-dialog::inner class="youtubePlayerDialog__inner relative">
            <template x-if="isOpen">
                <x-youtubePlayer::player :url="$url" />
            </template>
        </x-dialog::inner>
    </x-dialog::base>
@endpush