import googleMaps from './googleMaps';

export default (Alpine) => {
    Alpine.data('googleMaps', googleMaps);
}