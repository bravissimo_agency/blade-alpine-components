<button
    type="button"
    @click="close"
    {{ $attributes }}
>
    {!! $slot !!}
</button>