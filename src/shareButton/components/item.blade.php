@props([
    'type',
])

<li class="shareButtonItem">
    <a 
        :href="getUrl('{{ $type }}')"
        target="_blank"
        class="shareButtonItem__link"
    >
        {!! $slot !!}
    </a>
</li>