import client from '../../utils/apiClient';

export default ({
    errorMessage,
    termsMessage,
    endpoint,
    mustAcceptTerms,
    recapatcha,
    initialData,
    gtmData,
    useJson
}) => ({
    formData: {},

    errorMessage: errorMessage,

    termsMessage: termsMessage,

    endpoint: endpoint,

    mustAcceptTerms: mustAcceptTerms,

    recapatcha: recapatcha,

    errors: null,

    isLoading: false,

    termsAccepted: false,

    initialData: initialData,

    gtmData: gtmData,

    useJson: useJson,

    init () {
        if (this.initialData) {
            this.formData = { ...this.initialData };
        }
    },

    submitHandler () {
        if (this.isLoading) {
            return;
        }

        if (this.mustAcceptTerms && !this.termsAccepted) {
            this.$store.notifications.add({
                status: 'warning',
                message: this.termsMessage
            });

            return;
        }

        this.isLoading = true;

        try {
            this.submit();
        } catch (error) {
            this.showError(error);
        }
    },

    async getFormData () {
        if (this.useJson) {
            return JSON.stringify(this.formData);
        }

        const formData = new FormData();

        for (const key in this.formData) {
            if (this.formData[key]) {
                if (typeof this.formData[key] === 'object' && this.formData[key][0] && this.formData[key][0].size) {
                    const fileListAsArray = Array.from(this.formData[key]);

                    fileListAsArray.forEach(f => {
                        formData.append(`${key}[]`, f);
                    });
                } else {
                    const data = this.formData[key].file || this.formData[key];
                    formData.append(key, data);
                }
            }
        }

        if (this.recapatcha && window.grecaptcha) {
            const token = await window.grecaptcha.execute(this.recapatcha, { action: 'form' });
            formData.append('gRecaptchaResponse', token);
        }

        return formData;
    },

    async submit () {
        this.errors = null;

        const headers = {};

        if (this.useJson) {
            headers['Content-Type'] = 'application/json';
        }

        client(this.endpoint, {
            body: await this.getFormData(),
            headers: headers
        }).then(
            data => {
                if (this.gtmData) {
                    this.submitGtmEvent();
                }

                if (data.redirectTo) {
                    window.location.replace(data.redirectTo);
                    return;
                }    
                
                if (this.initialData) {
                    this.formData = { ...this.initialData };
                } else {
                    this.formData = {};
                }

                this.isLoading = false;

                if (data.message) {
                    this.$store.notifications.add({
                        status: 'success',
                        message: data.message
                    });
                }

                this.$dispatch('success');
            },
            error => {
                this.isLoading = false;
                this.errors = error.errors;
                this.showError(error);
            }
        );
    },

    showError (error) {
        let message = '';

        if (error.type === 'error' || error.type === 'validation_error') {
            message = (error && error.message) || this.errorMessage;
        } else {
            message = this.errorMessage;
        }

        this.$store.notifications.add({
            status: 'warning',
            message
        });
    },

    submitGtmEvent () {
        if (!window.dataLayer) {
            return;
        }

        const gtmFields = {
            event: this.gtmData.event,
            value: 'submitSuccess'
        };

        if (this.gtmData.fields) {
            this.gtmData.fields.forEach(field => {
                if (this.formData[field]) {
                    gtmFields[field] = this.formData[field];
                }
            });
        }

        window.dataLayer.push(gtmFields);
    }
});