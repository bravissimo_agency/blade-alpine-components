import dialog from './dialog';

export default (Alpine) => {
    Alpine.data('dialog', dialog);

    Alpine.store('dialog', {
        id: null,

        open (id) {
            this.id = id;
        },

        close () {
            this.id = null;
        }
    })
}