<div 
    x-data="dropdown"
    :class="{ 'isOpen': isOpen }"
    {{ $attributes->merge(['class' => 'dropdown']) }}
>
    {!! $slot !!}
</div>