import youtubePlayer from './youtubePlayer';

export default (Alpine) => {
    Alpine.data('youtubePlayer', youtubePlayer);
}