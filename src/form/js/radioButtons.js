export default () => ({
    selected: null,

    select (value) {
        this.selected = value;
    }
})