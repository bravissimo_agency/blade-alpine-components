@props([
    'for' => null,
])

<label
    for="{{ $for }}"
    {{ $attributes->merge(['class' => 'label inline-block']) }}
>
    {!! $slot !!}
</label>
