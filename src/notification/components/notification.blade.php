<div
    x-show="notification.visible"
    x-transition:enter="notification-enter"
    x-transition:enter-start="notification-enter-start"
    x-transition:enter-end="notification-enter-end"
    x-transition:leave="notification-leave"
    x-transition:leave-start="notification-leave-start"
    x-transition:leave-end="notification-leave-end"
    :class="`notification-${notification.status}`"
    class="notification"
>
    <div
        class="notification__inner inline-flex items-center cursor-pointer"
        @click="close(notification)"
    >
        <span
            :class="notification.status"
            class="notification__icon flex-shrink-0"
        >
            <svg
                x-show="notification.status === 'success'"
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                stroke-width="2"
                stroke-linecap="round"
                stroke-linejoin="round"
            ><path d="M22 11.08V12a10 10 0 1 1-5.93-9.14" /><polyline points="22 4 12 14.01 9 11.01" /></svg>

            <svg
                x-show="notification.status === 'warning'"
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                stroke-width="2"
                stroke-linecap="round"
                stroke-linejoin="round"
            ><circle
                cx="12"
                cy="12"
                r="10"
            /><line
                x1="12"
                y1="8"
                x2="12"
                y2="12"
            /><line
                x1="12"
                y1="16"
                x2="12.01"
                y2="16"
            /></svg>
        </span>

        <p
            class="notification__text"
            x-text="notification.message"
        ></p>
    </div>
</div>
