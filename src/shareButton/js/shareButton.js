export default () => ({
    isOpen: false,

    init () {
        document.addEventListener('keydown', (event) => {
            if (!this.isOpen) {
                return;
            }

            if (event.key === 'Escape') {
                this.close();
            }
        });
    },

    toggle () {
        this.isOpen ? this.close() : this.open();
    },

    open () {
        if (this.hasNativeShare()) {
            this.openNativeShare();
        } else {
            this.isOpen = true;
        }
    },

    close () {
        this.isOpen = false;
    },

    getUrl (type) {
        const title = encodeURIComponent(document.title);
        const url = encodeURIComponent(document.location.href);

        if (type === 'facebook') {
            return `https://www.facebook.com/sharer/sharer.php?u=${url}&title=${title}`;
        }

        if (type === 'twitter') {
            return `https://twitter.com/intent/tweet?text=${url}`;
        }

        if (type === 'linkedin') {
            return `https://www.linkedin.com/shareArticle?mini=true&url=${url}&title=${title}&source=${url}`;
        }

        if (type === 'mail') {
            return `mailto:?subject=${title}&body=${url}`;
        }
    },

    hasNativeShare () {
        return window.navigator.share;
    },

    openNativeShare () {
        window.navigator.share({
            title: document.title,
            url: window.location.href
        });
    }
})