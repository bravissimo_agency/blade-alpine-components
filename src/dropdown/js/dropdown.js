export default () => ({
    isOpen: false,

    init () {
        document.addEventListener('keydown', (event) => {
            if (!this.isOpen) {
                return;
            }

            if (event.key === 'Escape') {
                this.close();
            }
        });
    },

    toggle() {
        this.isOpen = !this.isOpen;
    },

    open () {
        this.isOpen = true;
    },

    close () {
        this.isOpen = false;
    }
});