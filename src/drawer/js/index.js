import drawer from './drawer';

export default (Alpine) => {
    Alpine.data('drawer', drawer);

    Alpine.store('drawer', {
        id: null,

        toggle (id) {
            this.id = this.id === id ? null : id;
        },

        open (id) {
            this.id = id;
        },

        close () {
            this.id = null;
        }
    })
}