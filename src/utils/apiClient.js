async function client (endpoint, { method, body, params, ...customConfig } = {}) {
    const config = {
        method: method ? method : body ? 'POST' : 'GET',
        ...customConfig,
        headers: customConfig.headers
    };

    if (body) {
        config.body = body;
    }

    const url = addQueryStrings(`/wp-json/api/${endpoint}`, params);

    const response = await fetch(url, config);
    const data = await response.json();

    if (response.ok) {
        return data;
    }

    return Promise.reject(data);
}

function addQueryStrings (url, params) {
    if (!params) {
        return url;
    }

    for (const [name, value] of Object.entries(params)) {
        url += url.includes('?') ? '&' : '?';
        url += `${name}=${value}`;
    }

    return url;
}

export default client;
