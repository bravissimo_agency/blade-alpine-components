@props([
    'label' => null,
    'name' => null
])

<div {{ $attributes->merge(['class' => 'formGroup']) }}>
    <div class="formGroup__inner">
        @if (!empty($label))
            <x-form::label :for="$name">
                {!! $label !!}
            </x-form::label>
        @endif

        {!! $slot !!}
    </div>
</div>
