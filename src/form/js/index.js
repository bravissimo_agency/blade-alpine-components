import checkbox from './checkbox';
import form from './form';
import radioButtons from './radioButtons';
import select from './select';

export default (Alpine) => {
    Alpine.data('checkbox', checkbox);
    Alpine.data('form', form);
    Alpine.data('radioButtons', radioButtons);
    Alpine.data('select', select);
}