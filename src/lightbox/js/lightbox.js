import PhotoSwipe from 'photoswipe/dist/photoswipe.js';
import PhotoSwipeUIDefault from 'photoswipe/dist/photoswipe-ui-default.js';

export default ({ images, settings }) => ({
    images: images,
    settings: settings || {},

    openLightbox (index = 0) {
        const el = document.querySelector('.pswp');

        const items = this.images.map(image => ({
            src: image.sizes.hd,
            w: image.sizes['hd-width'],
            h: image.sizes['hd-height'],
            title: image.caption
        }));

        const settings = {
            history: false,
            shareEl: false,
            bgOpacity: 0.8,
            showHideOpacity: true,
            timeToIdle: false,
            ...this.settings
        }

        const gallery = new PhotoSwipe(el, PhotoSwipeUIDefault, items, {
            ...settings,
            index
        });

        gallery.init();
    }
});