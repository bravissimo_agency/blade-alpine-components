@props([
    'items',
    'placeholder' => null,
    'optionLabel' => 'label',
    'optionValue' => 'value',
    'buttonClass' => '',
    'listClass' => '',
    'itemClass' => '',
    'multiple' => false,
    'showClear' => false
])

<div
    x-data="select({
        items: {{ json_encode($items) }},
        optionLabel: '{{ $optionLabel }}',
        optionValue: '{{ $optionValue }}',
        multiple: {{ json_encode($multiple) }}
    })"
    x-modelable="selectedValue"
    :class="{ 'isOpen': isOpen }"
    {{ $attributes->merge(['class' => 'select relative']) }}
>
    <button 
        type="button"
        @click="toggle"
        class="select__button {{ $buttonClass }}"
    >
        <span 
            x-text="selected && (!multiple || selected.length > 0) ? selectedLabel : '{{ $placeholder }}'"
            class="select__label"
        ></span>

        <span class="select__iconsHolder flex items-center flex-shrink-0">
            @if ($showClear)
                <svg x-show="selected" @click.stop="clear" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="select__clear flex-shrink-0"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
            @endif

            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="select__arrow flex-shrink-0"><polyline points="6 9 12 15 18 9"></polyline></svg>
        </span>
    </button>

    <ul 
        x-show="isOpen"
        class="select__list {{ $listClass }}"
        x-transition.duration.250ms
        @click.outside="close"
        x-cloak    
    >
        @foreach ($items as $item)
            <li 
                class="select__item  {{ $itemClass }}"
                :class="{ 'isActive': isActive({{ json_encode($item) }}) }"
                @click="selectItem({{ json_encode($item) }})"    
            >
                {{ $item->{$optionLabel} }}
            </li>
        @endforeach
    </ul>
</div>