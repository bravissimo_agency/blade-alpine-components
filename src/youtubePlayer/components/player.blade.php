@props([
    'url',
    'width' => 1920,
    'height' => 1080,
])

<div x-data="youtubePlayer({
    url: '{{ $url }}',
    width: {{ $width }},
    height: {{ $height }}
})">
    <div class="embedWrapper">
        <div id="youtubePlayerEmbeed"></div>
    </div>
</div>