@props([
    'message'
])

<div 
    x-show="showCookieNotification"
    class="cookieNotice notification"
    x-transition:enter="notification-enter"
    x-transition:enter-start="notification-enter-start"
    x-transition:enter-end="notification-enter-end"
    x-transition:leave="notification-leave"
    x-transition:leave-start="notification-leave-start"
    x-transition:leave-end="notification-leave-end"
    x-cloak
>
    <div class="notification__inner inline-flex items-center">
        <span
            id="cookieNotice__text"
            class="cookieNotice__text"
        >
            {!! $message !!}
        </span>

        <x-notification::close 
            @click="closeCookieNotification"
        />
    </div>
</div>
