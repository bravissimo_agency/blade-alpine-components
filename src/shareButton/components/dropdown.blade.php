<div
    x-show="isOpen"
    class="shareButtonDropdown"
    x-transition.duration.350ms.scale.0.origin.top.opacity
    x-cloak
>
    <ul class="shareButtonDropdown__list">
        {!! $slot !!}
    </ul>
</div>