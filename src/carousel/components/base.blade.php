@props([
    'slides',
    'fullSize' => null,
    'settings' => null
])

<div
    x-data="carousel({
        slideCount: {{ $slides->count() }},
        settings: {{ json_encode($settings ?? null) }}
    })"
    {{ $attributes->merge(['class' => 'carousel ' . (!empty($fullSize) ? 'isFullSize' : '')]) }}
>
    {!! $slot !!}
</div>