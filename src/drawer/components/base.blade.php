@props([
    'name',
    'transitionName' => null,
    'activeBodyClass' => '',
    'defaultOpen' => false
])

<div
    x-data="drawer({
        id: '{{ $name }}',
        activeBodyClass: '{{ $activeBodyClass }}',
        defaultOpen: {{ json_encode($defaultOpen) }}
    })"
    x-show="isOpen"
    x-transition:enter="{{ $transitionName ?? $name }}-enter"
    x-transition:enter-start="{{ $transitionName ?? $name }}-enter-start"
    x-transition:enter-end="{{ $name }}'-enter-ed"
    x-transition:leave="{{ $transitionName ?? $name }}-leave"
    x-transition:leave-start="{{ $transitionName ?? $name }}-leave-start"
    x-transition:leave-end="{{ $transitionName ?? $name }}-leave-end"
    :aria-hidden="isOpen ? 'false' : 'true'"
    id="{{ $name }}"
    role="region"
    tabindex="-1"
    x-cloak
    {{ $attributes->merge(['class' => 'drawer']) }}
>
    {!! $slot !!}
</div>