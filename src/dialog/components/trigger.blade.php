@props([
    'id'
])

<button
    type="button"
    @click="$store.dialog.open('{{ $id }}')"
    {{ $attributes }}
    x-data
>
    {!! $slot !!}
</button>