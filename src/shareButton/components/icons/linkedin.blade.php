<x-shareButton::icons.icon class="linkedin">
    <svg xmlns="http://www.w3.org/2000/svg" class="shareButtonIcon__svg" width="24" height="24" data-name="Layer 21" viewBox="0 0 24 24">
        <path fill="currentColor" d="M3 9h4v11H3z"></path>
        <circle fill="currentColor" cx="5" cy="5" r="2"></circle>
        <path fill="currentColor" d="M16.5 8.25A4.473 4.473 0 0 0 13 9.953V9H9v11h4v-7a2 2 0 0 1 4 0v7h4v-7.25a4.5 4.5 0 0 0-4.5-4.5z"></path>
    </svg>
</x-shareButton::icons.icon>
