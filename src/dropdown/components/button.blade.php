<button 
    type="button"
    @click="toggle"
    {{ $attributes->merge(['class' => 'dropdown__list']) }}
>
    {!! $slot !!}
</button>